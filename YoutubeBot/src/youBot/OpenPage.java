package youBot;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OpenPage {
    static WebDriver driver;
    static Wait<WebDriver> wait;
    static String user = "";
    static String comment = "";

    public static void main(String[] args) {
    	
    	Properties prop = new Properties();
    	InputStream input = null;
    	
    	try {

    		input = new FileInputStream("settings/config.properties");

    		// load a properties file
    		prop.load(input);

    		// get the property value and print it out
    		user = prop.getProperty("user");
    		comment = prop.getProperty("comment");
    		
    		System.out.println("User name = "+user);
    		System.out.println("Comment = "+comment);

    	} catch (IOException ex) {
    		ex.printStackTrace();
    	} finally {
    		if (input != null) {
    			try {
    				input.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	    	
    	ProfilesIni profile = new ProfilesIni();
    	FirefoxProfile myprofile = profile.getProfile("selenium");
    	driver = new FirefoxDriver(myprofile);
        wait = new WebDriverWait(driver, 5);
        driver.get("http://www.youtube.com/");

        boolean result;
        try {
            result = firstPageContainsQAANet();
        } catch(Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            driver.close();
        }

        System.out.println("Work " + (result? "passed." : "failed."));
        if (!result) {
            System.exit(1);
        }
    }

    private static boolean firstPageContainsQAANet() {
        
    	//open channel
        driver.get("http://www.youtube.com/user/"+user+"/videos");
        
        // Wait for open to complete
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                System.out.println("Opening channel...");
                return webDriver.findElement(By.xpath(".//*[@id='channels-browse-content-grid']/li[1]/div/div[1]/div[2]/div/ul/li[2]")) != null;
            }
        });
    	
        //check last clip   
        if (driver.findElement(By.xpath(".//li[1]/div/div[1]/div[2]/div/ul/li[2]"))
        		.getText().contains("минуты") ||
        		driver.findElement(By.xpath(".//li[1]/div/div[1]/div[2]/div/ul/li[2]"))
        		.getText().contains("минуту") ||
        		driver.findElement(By.xpath(".//li[1]/div/div[1]/div[2]/div/ul/li[2]"))
        		.getText().contains("минут") ||
        		driver.findElement(By.xpath(".//li[1]/div/div[1]/div[2]/div/ul/li[2]"))
        		.getText().contains("час") ||
        		driver.findElement(By.xpath(".//li[1]/div/div[1]/div[2]/div/ul/li[2]"))
        		.getText().contains("часов")) {
        	System.out.println("Found a new movie");
        	
        	//open clip
        	driver.findElement(By.xpath(".//li[1]/div/div[1]/div[1]/span/a")).click();
        	
            // Wait for open to complete
            wait.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    System.out.println("Opening video...");
                    return webDriver.findElement(By.id("watch-headline-title")) != null;
                }
            });
        	
        	//open comment input
            // Wait for open to comments input
            wait.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    System.out.println("Opening coments...");
                    return webDriver.findElement(By.id("comment-simplebox-create-comment")) != null;
                }
            });
        	
        	//commentate
        	driver.findElement(By.xpath(".//*[@id='comment-section-renderer']/div[1]/div[2]"))
        		.click();
        	driver.findElement(By.xpath(".//*[@id='comment-simplebox']/div[2]/div[2]"))
        		.sendKeys(comment);
        	System.out.println("Comment: "+comment);
        	
        	//submit
        	driver.findElement(By.xpath(".//*[@id='comment-simplebox']/div[3]/div/button[2]"))
        		.click();
        	
            // Wait for our comment
            wait.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    System.out.println("Opening coments...");
                    return webDriver
	            		.findElement(By
	            		.xpath(".//*[@id='comment-section-renderer-items']/section[1]/div[1]/div[2]/div[2]/div[1]"))
	            		.getText()
	            		.contains(comment);
                }
            });
        	    	
        } else {
        	System.out.println("Нет новых видео");
        	driver.close();
        }
        	
        // Look for QAAutomation.net in the results
        return driver
    		.findElement(By
				.xpath(".//*[@id='comment-section-renderer-items']/section[1]/div[1]/div[2]/div[2]/div[1]"))
    		.getText()
    		.contains(comment);
    }
}